<h1 align="center">Frontend Development</h1>

### Description

Tests and practice work for improving Frontend skills at Corra.

From vanilla Javascript to React, this project takes a tour through Frontend development
practices, styles, and examples. All in hopes to improve both Javascript and 
React knowledge and routine.

Please contribute or comment.


Facts
-----

* Version 0.1.0
* [Repository on BitBucket](Link)
* [Confluence Documents](Link)


Getting Started
---------------

```shell
$ git clone git@bitbucket.org:rmcorra/corra-js.git
```


### Requirements
 * [Git](https://git-scm.com/)
 * [React](https://react.dev/)
 * [Docker](https://www.docker.com/)


### Known Issues

* None at this time


### Installation and Automation

Run `make` or `make build` to compile your app. This will use docker build to build a
production like environment.

For other environments, run the following:

**Local Project [Test Environment]**
```shell
 $ make test/build
 $ make test/up
```

**Local Project [Development Environment]**
```shell
 $ make dev/build
 $ make dev/up
```

Run `make clean` to remove all stopped containers.

Run `make dev` or `make test` to use docker build to both build and deploy the 
containers for either a test or development architecture. This runs both the 
`build` and `up` commands as one to simplify the build.

Run `make down` to stop all currently running images and volumes.

Run `make uninstall` to remove all images, containers, and networks. Careful with
this if you have other running Docker projects.

Run `make version` to output the current project version. May be important for included tasks.

Run `make help` to get these targets and their descriptions.


Support
-------
If you have any issues with the project, open an issue on BitBucket.


Developer
---------
* **Rye Miller** - *Initial Work*

See also a list of contributors who contributed to this project.


License
-------

This project/code is released under a proprietary license.


Copyright
---------

(c) 2022-Present Corra Technologies, Inc. All Rights Reserved
