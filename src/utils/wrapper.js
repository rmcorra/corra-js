
/** @desc Use before each component. **/
const Wrapper = props => {
  return props.children;
}

export default Wrapper;
