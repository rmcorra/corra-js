

export const PENDING_STATUS = "pending"
export const SUCCESS_STATUS = "success"
export const FAILED_STATUS = "failed"

export const ENV_DEVELOPMENT = "development"
export const breakline = "\r\n"

export const VALIDATION_REGEXPS = {
  phones: /^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s/0-9]{6,16}$/g,
  email: /^(\S+@\S+\.\S+)$/gm,
  first_name: /^([\u00C0-\u017Fa-zA-Z']+)/g,
  name_with_space: /^([\u00C0-\u017Fa-zA-Z.\-\s']+)$/g,
  last_name: /^([\u00C0-\u017Fa-zA-Z']+)/g,
  hasNoSpace: /^\S+$/,
}
