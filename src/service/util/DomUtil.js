/**
 * @fileoverview A toolset for providing improved DOM support across Javascript.
 * @package CorraJs.Util
 */

'use strict';

export class DomUtil {

  /**
   * @param element
   */
  static clearElement(element) {
    while (element.firstChild) {
      element.removeChild(element.firstChild);
    }
  }

  /**
   * @param html
   * @returns {ChildNode}
   */
  static createHtmlElement(html) {
    const template = document.createElement("template");
    template.innerHTML = html.trim();

    return template.content.firstChild;
  }

  /**
   * @param element
   * @param eventName
   * @param selector
   * @param handler
   * @returns {{remove: remove}}
   */
  static delegate(element, eventName, selector, handler) {
    const eventListener = function (event) {
      let target = event.target;
      while (target && target !== this) {
        if (target.matches(selector)) {
          handler.call(target, event);
        }
        target = target.parentNode;
      }
    }
    element.addEventListener(eventName, eventListener);

    return {
      remove: function () {
        element.removeEventListener(eventName, eventListener);
      }
    }
  }

  /**
   * @param callback
   */
  static documentReady(callback) {
    document.addEventListener("DOMContentLoaded", callback);
    if (document.readyState === "interactive" || document.readyState === "complete") {
      document.removeEventListener("DOMContentLoaded", callback);
      callback();
    }
  }

  /**
   * @param name
   * @param element
   * @returns {string}
   * @see setCustomProperty()
   */
  static getCustomProperty(name, element = document.documentElement) {
    return getComputedStyle(element).getPropertyValue("--" + name).trim();
  }

  /**
   * @param context
   * @returns {{}}
   */
  static getFormInputValues(context) {
    const inputs = context.querySelectorAll("input, select");
    const values = {};

    inputs.forEach((input) => {
      if (input.type === "checkbox") {
        values[input.id] = !!input.checked
      } else {
        values[input.id] = input.value
      }
    });

    return values;
  }

  /**
   * @param newChild
   * @param refChild
   */
  static insertAfter(newChild, refChild) {
    refChild.parentNode.insertBefore(newChild, refChild.nextSibling);
  }

  /**
   * @desc Returns whether the Browser is in dark mode or not.
   * @returns {boolean}
   */
  static isDarkMode() {
    return !!(window.matchMedia && window.matchMedia("(prefers-color-scheme: dark)").matches);
  }

  /**
   * @param element
   * @returns {boolean}
   * @see https://stackoverflow.com/questions/123999/how-can-i-tell-if-a-dom-element-is-visible-in-the-current-viewport
   */
  static isElementInViewport(element) {
    const rect = element.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  /**
   * @param element
   * @returns {boolean}
   * @see https://stackoverflow.com/questions/19669786/check-if-element-is-visible-in-dom
   */
  static isElementVisible(element) {
    return !!(element.offsetWidth || element.offsetHeight || element.getClientRects().length);
  }

  /**
   * @desc Supports the loading of CSS files using Javascript.
   * @param src
   */
  static loadCss(src) {
    const element = document.createElement("link");

    element.setAttribute("rel", "stylesheet");
    element.setAttribute("type", "text/css");
    element.setAttribute("href", src);

    document.getElementsByTagName("head")[0].appendChild(element);
  }

  /**
   * @param src
   */
  static loadJs(src) {
    const element = document.createElement("script");

    element.setAttribute("type", "text/javascript");
    element.setAttribute("src", src);

    document.getElementsByTagName("head")[0].appendChild(element);
  }

  /**
   * @param callback
   */
  static onDocumentReady(callback) {
    this.documentReady(callback);
  }

  static openLinksToBlank() {
    const links = document.links;

    for (let i = 0; i < links.length; i++) {
      const target = links[i].target;
      if (links[i].hostname !== window.location.hostname && target !== "_self") {
        links[i].target = "_blank";
      }
    }
  }

  /**
   * @param element
   */
  static removeHtmlElement(element) {
    element.parentNode.removeChild(element);
  }

  static setCustomProperty(name, value, element = document.documentElement) {
    element.style.setProperty("--" + name, value.trim());
  }

  /**
   * Returns true or false whether the Browser supports dark or light modes.
   * @returns {boolean}
   */
  static supportsPreferredMode() {
    return window.matchMedia &&
      (window.matchMedia("(prefers-color-scheme: dark)").matches || window.matchMedia("(prefers-color-scheme: light").matches);
  }
}
