/**
 * @fileoverview A script for offering HTTP support through Javascript.
 * @package CorraJs.Util
 */

'use strict';

/**
 * @desc
 */
export class HttpUtil {

  static getUrlParam(name, url = window.location.href) {
    return new URL(url).searchParams.get(name);
  }
}
