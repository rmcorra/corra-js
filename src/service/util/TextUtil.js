/**
 * @fileoverview A script for offering Text utilities within Javascript.
 * @package CorraJs.Util
 */

'use strict';

/**
 * @type {{"\"": string, "&": string, "'": string, "<": string, ">": string}}
 */
const entityMap = {
  "&": "&amp;",
  "<": "&lt;",
  ">": "&gt;",
  '"': '&quot;',
  "'": '&#39;'
}

/**
 * @desc TextUtil is a collection of utilities for improving content output.
 */
export class TextUtil {

  /**
   * @param str
   * @param maxLength
   * @returns {*|string}
   */
  static cut(str, maxLength) {
    if (str.length > maxLength) {
      return str.substring(0, maxLength) + "_";
    } else {
      return str;
    }
  }

  /**
   * @param raw
   * @returns {string}
   */
  static escapeHtml(raw) {
    // const escapeHtml = (unsafe) => {
    //   return unsafe.replaceAll('&', '&amp;').replaceAll('<', '&lt;').replaceAll('>', '&gt;').replaceAll('"', '&quot;').replaceAll("'", '&#039;');
    // }

    return String(raw).replace(/[&<>"']/g, (sym) => {
      return entityMap[sym];
    });
  }

  /**
   * @param raw
   * @returns {string}
   */
  static nl2br(raw) {
    return String(raw).replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, "$1<br />");
  }

  /**
   * @param html
   * @returns {string}
   */
  static stripHtml(html) {
    let tmp = document.createElement("div");
    tmp.innerHTML = html;

    return tmp.innerText;
  }

  /**
   * @param text
   * @returns {*}
   * @see https://stackoverflow.com/questions/1500260/detect-urls-in-text-with-javascript
   */
  static urlify(text) {
    // const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
    const urlRegex = /(https?:\/\/\S+)/g;

    return text.replace(urlRegex, function(url) {
      console.log("url: ", url);
      return '<a href="' + url + '">' + url + '</a>';
    });

    // const text = "this is a text"
    // const html = urlify(text);
    // console.log(html);
  }

  /**
   * @param str
   * @param maxLength
   * @returns {string}
   */
  static wrap(str, maxLength) {
    const words = str.split(" ");
    let lines = [];
    let line = "";

    for (let i = 0; i < words.length; i++) {
      const word = words[i];
      if (line.length + word.length < maxLength) {
        line += word + " "
      } else {
        lines.push(line.trim());
        line = word + " ";
      }
    }

    lines.push(line.trim());
    return lines.join("\n");
  }
}
