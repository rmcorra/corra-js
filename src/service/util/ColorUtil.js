/**
 * @fileoverview A script for extending the color tools in Javascript.
 * @package CorraJs.Util
 */

'use strict';

/**
 * @desc A color utility for improving conversions and tools in Javascript.
 */
export class ColorUtil {

  /**
   * @param hex
   * @returns {{r: number, b: number, g: number}}
   */
  static hexToRgb(hex) {
    let r = 0, g = 0, b = 0;

    if (hex.length === 4) {
      r = hex[1] + hex[1];
      g = hex[2] + hex[2];
      b = hex[3] + hex[3];
    } else if (hex.length === 7) {
      r = hex[1] + hex[2];
      g = hex[3] + hex[4];
      b = hex[5] + hex[6];
    }

    return {
      r: parseInt(r, 16),
      g: parseInt(g, 16),
      b: parseInt(b, 16)
    }
  }

  /**
   * @param r
   * @param g
   * @param b
   * @returns {string}
   */
  static rgbToHex(r, g, b) {
    if (typeof r === "object" && !g && !b) {
      b = r.b.toString(16);
      g = r.g.toString(16);
      r = r.r.toString(16);
    } else {
      r = r.toString(16);
      g = g.toString(16);
      b = b.toString(16);
    }

    if (r.length === 1) r = "0" + r;
    if (g.length === 1) g = "0" + g;
    if (b.length === 1) b = "0" + b;

    return "#" + r + g + b;
  }
}
