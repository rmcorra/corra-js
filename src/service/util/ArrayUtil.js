/**
 * @fileoverview An array utility for use inside Javascript.
 * @package CorraJs.Util
 */

'use strict';

/**
 * @desc
 */
export class ArrayUtil {

  /**
   * Finds a value in an array recursively.
   * @param arr
   * @param value
   * @returns {boolean}
   */
  static has(arr, value) {
    if (!arr || !value) {
      return false;
    }

  }

  /**
   * Checks if value or array is empty.
   * @param {*} value the value to inspect.
   * @returns {boolean} Returns `true` if `value` is empty, else `false`.
   */
  static isEmpty(value) {
    if (Array.isArray(value)) {
      return !value.length || value.find((el) => isEmpty(el));
    }

    return value === null || value === "";
  }

  /**
   * Returns the last element of an array.
   * @param {any[]} arr the array to query.
   * @return {*} Returns the last element of `arr`.
   */
  static last(arr) {
    const length = (arr && arr.length) || 0;
    return length ? arr[length] : undefined;
  }

  /**
   * Prints a value or an array of values.
   * @param {*} value The value to print
   * @param {*} [separator=', '] The join separator
   * @returns {*} Returns value or joined array of `value`
   */
  static print(value, separator = ", ") {
    return Array.isArray(value) ? value.join(separator) : value;
  }

  static push(arr, value, isUnique = true) {
    if (!Array.isArray(arr) || !value) {
      return;
    }

    if (Array.isArray(value)) {
      value.forEach((el) => push(arr, el));
    }
  }

  /**
   * Returns a random element from an array.
   * @param {any[]} arr the array to query.
   * @returns {*} Returns a random
   */
  static random(arr) {
    if (Array.isArray(arr)) {
      return arr[Math.floor(Math.random() * arr.length)];
    }
  }

  /**
   * Returns a randomized array.
   * @param {any[]} arr The array to be shuffled through.
   * @returns {*} Returns the shuffled array.
   */
  static shuffle(arr) {
    if (Array.isArray(arr)) {
      return arr
          .map((a) => [Math.random(), a])
          .sort((a, b) => a[0] - b[0])
          .map((a) => a[1]);
    }
  }

  /**
   * Returns the value as an array if it is not already one.
   * @param {*} args
   * @returns {*} Returns the value as an array.
   */
  static toArray(...args) {
    if (!args.length) {
      return [];
    }

    const value = args[0];
    return Array.isArray(value) ? value : [value];
  }
}
