/**
 * @fileoverview An array utility for use inside Javascript.
 * @package CorraJs.Util
 */

'use strict';

/**
 * @desc
 */
export const Corraps = {};

// Only characters
Corraps.onlyChars = (s) => {
  return s.replace(/[^\w]/g, ``);
}

Corraps.noSpace = (s) => {
  return s.replace(/\s+/g, ``);
}

Corraps.singleSpace = (s) => {
  return s.replace(/\s+/g, ``);
}

Corraps.cleanUsername = (s) => {
  return s.replace(/[^a-z0-9]+/gi, ``).replace(/ +/g, ` `).trim();
}

Corraps.noSpace = (s) => {
  return s.replace(/\s+/g, ``).trim()
}

Corraps.removeMultipleEmptyLines = (s, level = 5) => {
  let ns = [];
  let charge = 0;
  let split = s.split(`\n`);

  for (let line of split) {
    if (line.trim() === ``) {
      if (charge < level) {
        ns.push(line);
      }
      charge += 1;
    } else {
      charge = 0;
      ns.push(line);
    }
  }

  return ns.join(`\n`);
}

// remove empty lines from the start
Corraps.removePreEmptyLines = (s) => {
  let split = s.split(`\n`);
  let counter = 0;

  for (let line of split) {
    if (line.trim()) {
      return split.splice(counter).join(`\n`);
    } else {
      counter += 1;
    }
  }
}

// no empty lines
Corraps.singleLineBreak = (s) => {
  return s.replace(/(\n\s*){2,}/g, `\n\n`).replace(/ +/g, ` `).trim();
}

// max 1 empty line
Corraps.doubleLinebreak = (s) => {
  return s.replace(/(\n\s*){3,}/g, `\n\n`).replace(/ +/g, ` `).trim();
}
