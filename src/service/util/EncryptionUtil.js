/**
 * @fileoverview A script for managing encryption keys across Javascript.
 * @package CorraJs.Util
 */

'use strict';

/**
 * @desc EncryptionUtil() is a toolset for managing encryption keys in javascript.
 */
export class EncryptionUtil {

  // creates an encryption key for storage
  static createKey(length = 12) {
    let key = "";
    const iterations = length / 12;
    for (let i = 0; i < iterations; i++) {
      key += Math.floor(1000000000000000 + Math.random() * 9000000000000000)
        .toString(36)
        .substring(0, 12);
    }
    return key.substring(0, length);
  }
}
