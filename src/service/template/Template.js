/**
 * @fileoverview A script for replacing HTML elements using Javascript.
 * @package CorraJs.Template
 */

'use strict';

/**
 * @desc Template() class for replacing and outputting HTML elements using Javascript.
 */
export class Template {
  // context is template or content
  constructor(context) {
    if (context instanceof HTMLElement) {
      this.content = "";
      for (const childNode of context.content.childNodes) {
        if (childNode.nodeType === Node.TEXT_NODE) {
          this.content += childNode.wholeText;
        } else if (childNode.nodeType === Node.ELEMENT_NODE) {
          this.content += childNode.outerHTML;
        }
      }
    } else if (typeof context === "string" || context instanceof String) {
      this.content = context;
    }
  }

  /**
   * @param replacements
   * @returns {string}
   */
  render(replacements) {
    return this.content.replace(/\${(.*?)}/g, (toReplace, key) => {
      if (replacements[key] === undefined) {
        return toReplace;
      } else {
        return replacements[key];
      }
    });
  }
}
