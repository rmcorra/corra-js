/**
 * @fileOverview International language support.
 * @package CorraJs.I18n
 */

'use strict';

/**
 * @desc @TODO write a description.
 */
export class I18n {

  constructor(props = {}) {
    this.props = {
      locale: null,
      fallbackLang: "en" // locale will search for translation or default to this
    }

    Object.assign(this.props, props)
    this.locale = this.props.locale

    if (!this.locale) {
      const htmlLang = document.documentElement.getAttribute("lang");
      if (htmlLang) {
        this.locale = htmlLang;
      }
      if (!this.locale) {
        this.locale = navigator.language;
      }
    }

    this.lang = this.locale.substring(0, 2);
    this.translations = {};
  }

  load(dictionary) {
    let fetchPromises = [];

    for (const lang in dictionary) {
      if (dictionary.hasOwnProperty(lang)) {
        if (!this.translations[lang]) {
          this.translations[lang] = {};
        }

        const translations = dictionary[lang];


      }
    }
  }
}
