Corra Publicis Sapient - Javascript Notifications <sup>`v0.1.0`</sup>
=====================================================================

A lightweight, fullstack JavaScript framework for keeping the lights on.


Getting Started
---------------

### Basic Usage
```shell
$ yarn add -g @corratech/corra-js
```

