/**
 * @fileoverview A tool for providing improved Notifications using Javascript.
 * @package CorraJs.Notification
 */

'use strict';

export class Notifications {

  requestPermission() {
    Notification.requestPermission((permission) => {
      console.log(permission);
    });
  }

  /**
   * @param title
   * @param body
   */
  show(title, body) {
    new Notification(title, {body: body});
  }
}
