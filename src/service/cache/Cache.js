/**
 * @fileoverview A script for setting and getting Cache keys.
 * @package CorraJs.Cache
 */

'use strict';

/**
 * @desc
 */

export class Cache {

  // @TODO update this.props
  constructor(props = {}) {
    this.props = {
      clearInterval: 30000
    }

    Object.assign(this.props, props)

    if (props.clearInterval && props.clearInterval > 0) {
      setInterval(() => {
        this.flush()
      }, props.clearInterval)
    }

    this.cache = {}
  }

  // clear a cache block
  clear(key) {
    this.cache[key] = undefined;
  }

  // flush the cache storage
  flush() {
    this.cache = {}
  }

  // retrieve a cache block
  get(key) {
    return this.cache[key];
  }

  // assign a cache block
  put(key, value) {
    this.cache[key] = value;
  }
}
