
import React from "react";

import './App.css';

import ScreenLayout from './components/layout';

const App = () => (
  <>
    <ScreenLayout />
  </>
);

export default App;
