import React from "react";

const Hero = (props) => {
  // const changeLanguage = () => {
  //   i18n.changeLanguage(i18n.language === 'en' ? 'de' : 'en')
  // }
  const heading = props.headingOne;

  return (
    <section className="text-gray-700 body-font">
      <div className="container mx-auto flex md:flex-row flex-col px-5 py-24 items-center">
        <div className="flex flex-col lg:flex-grow lg:pr-24 md:w-1/2 md:pr-16 md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
          <h1 className="title-font sm:text-4xl text-2xl mb-4 font-medium text-gray-900">
            {heading}
          </h1>
          <p className="mb-8 leading-relaxed">
            Copper mug try-hard pitchfork pour-over freegan heirloom neutra air
            plant cold-pressed tacos poke beard tote bag. Heirloom echo park
            mlkshk tote bag selvage hot chicken authentic tumeric truffaut
            hexagon try-hard chambray.
          </p>
          <div className="flex justify-center">
            <button className="inline-flex px-6 py-2 bg-gray-300 border-0 focus:outline-none hover:bg-gray-900 rounded text-lg text-white">Link One</button>
            <button className="ml-4 inline-flex px-6 py-2 bg-gray-900 border-0 focus:outline-none hover:bg-gray-600 rounded text-lg text-white">Link Two</button>
          </div>
        </div>
        <div className="w-5/6 lg:max-w-lg lg:w-full md:w-1/2">
          <img alt="Dummy Hero Image" className="object-cover object-center rounded" src="https://fakeimg.pl/600x400?text=CorraJs&font=bebas" />
        </div>
      </div>
    </section>
  );
};

export default Hero;
