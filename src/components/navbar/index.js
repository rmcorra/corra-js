
import React from "react";

import './navbar.module.css';

const Navbar = () => {
  return (
    <nav>
      <ul>
        <li><a href="/">Home</a></li>
        <li><a href="/">Style Guide</a></li>
        <li><a href="/">Store</a></li>
        <li><a href="/">Scores</a></li>
        <li><a href="/">Contacts</a></li>
      </ul>
    </nav>
  )
}

export default Navbar
