import React from "react";

import logo from "../../assets/img/logo-dark.svg";





const Header = (props) => {
  const {title, link} = props;
  return (
    <header className="text-gray-700 body-font border-b border-gray-200">
      <div className="container mx-auto flex flex-wrap p-5 flex-col md:flex-row items-center">
        <a className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0" href={link} target="_blank">
          <img className="ml-3" alt="CorraJs Logo" src={logo} width="40%"/>
          <span className="ml-3 text-sm">{title}</span>
        </a>
        <nav className="md:ml-auto flex flex-wrap items-center text-base justify-center">
          <a className="mr-5 hover:text-gray-900">First Link</a>
          <a className="mr-5 hover:text-gray-900">Second Link</a>
          <a className="mr-5 hover:text-gray-900">Third Link</a>
        </nav>
      </div>
    </header>
  );
};

export default Header;
