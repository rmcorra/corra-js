import React from "react";

const Button = (props) => {

  const {url, linkTitle} = props;

  return (
    <button className="inline-flex items-center px-3 py-1 bg-gray-200 border-0 focus:outline-none hover:bg-gray-400 rounded mt-4 md:mt-0">
      <a href={url} target="_blank">
        {linkTitle}
      </a>
    </button>
  );
};

export default Button;
