import React from "react";

const ComponentG = ({ one }) => {

  return (
    <section className="container mx-auto">
      <p>{one}</p>
    </section>
  );
};

export default ComponentG;
