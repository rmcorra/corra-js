#
# Makefile for Corra JS
# Version 0.1.0 [2023-10-06]
# Copyright (c) 2023, Corra Technologies
#

SHELL         := /bin/bash
PROJECT       := corra-js
VERSION       := 0.1.0

-include tasks/Makefile.*

.DEFAULT_GOAL := all

build:
	make docker/build

stats:
	@docker stats $(docker ps --format='{{.Names}}')

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  clean       removes all stopped containers."
	@echo "  up          to run a React development environment."
	@echo "  down        removes the currently running images and volumes."
	@echo "  uninstall   removes all images, containers, and networks."
	@echo "  version     outputs the currently running project version."

up:
	make docker/up

down:
	make docker/down

clean:
	# make docker/remove

uninstall:
	make docker/prune

version:
	@echo ${VERSION}
