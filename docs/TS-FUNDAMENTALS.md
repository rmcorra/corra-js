Corra TypeScript Fundamentals
=============================

This document is an opinionated guide to the fundamentals of the TypeScript programming
language.


---


TypeScript allows you to write JavaScript the same way you always do. TS compiles to plain, vanilla javascript.

Fully compatible Javascript.

Javascript is a dynamic language, forgiving in how you use variables.

Typescript catches problems by adding typing, static typing on top of Javascript.

Superset of Javascript. Extends JS rather than creating a new language.

TypeScript = Javascript

Created by Microsoft.

