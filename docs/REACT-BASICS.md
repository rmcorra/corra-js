React Basics
============


App Component
-------------

Tha `App` component is the entry point of a React application. It renders the main
layout of the application.


### Naming Conventions
 
 * Components should follow **PascalCase**
   * Types, Interfaces, and Javascript Classes
   * Other entities can be **camelCase**
   * Files of these (Types, Interfaces, Classes) should follow this convention
     * Helpers, Utilities, and Config files should follow PascalCase
   * A component's file should always bear the same name as the component (for its default export)
   * Only ONE component per file
     * If it is a tiny component and must remain private it may, though not suggested, include two per file
 * Components names must be **unique** across the entire application
   * Same names constitute either bad naming convention or both components do the same thing and should be refactored
   * It is better to include three components with long names over three components with identical names in separate contexts
 * Component names must be **precise** and **relevant**
   * Avoid names like Block, Component, Entry, or generic words too hard to understand their contexts
 * Component names MUST be consistent
   * Consistency appears when keeping same logic across and application for the same matter
   * Consistency is choosing the same names for data entities or UI elements, and sticking to it
     * even when naming feels wrong, if it is consistent - do it
     * refactoring may work too
     * e.g. is there a difference between `Dialog` and `Modal`, choose one, or both like `DialogModal` and stick to it
 * Component names MUST inform as much as possible their function and what data or model they render
   * e.g. `<Purpose>/<Action><Context>/<Data><UIElement>`
     * Purpose and Action are what the actual thing does
     * Context/Work
       * What is it working on
       * `CloseTicketModal`
     * UIElement - What kind of element is it? Is it quickly identifiable to the main rendering of the component?


### Implementation


