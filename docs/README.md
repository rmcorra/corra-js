Corra JS Documentation
======================

Best practices and code scenarios for React and composable architectures.


Getting Started
---------------

1. [Basics](https://bitbucket.org/rmcorra/corra-js/src/develop/docs/REACT-BASICS.md)
2. [Recipes](https://bitbucket.org/rmcorra/corra-js/src/develop/docs/REACT-RECIPES.md)
3. [TypeScript](https://bitbucket.org/rmcorra/corra-js/src/develop/docs/TS-FUNDAMENTALS.md)


